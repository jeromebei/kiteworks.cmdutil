var gulp = require('gulp');
var bump = require('gulp-bump');
var exec = require('child_process').exec;
var fs = require('fs');
var git = require('gulp-git');
//======================================================================================================================
tools = ["kwupload",
  "kwmd",
  "kwls",
  "kwtest",
  "kwmail",
  "kwrefresh",
  "kwdownload",
  "kwme",
  "kwrd",
  "kwdel",
  "kwsearch",
  "kwinvite",
  "kwrequest",
  "kwwatch"
];
//======================================================================================================================
function getVersion() {
  return JSON.parse(fs.readFileSync('./version.json')).version;
}
//======================================================================================================================
function buildTool(tool, cb) {
  console.log("building " + tool + " for mac");
  exec('go build -o dist/mac/' + tool + " src/" + tool + "/" + tool + ".go", function(err, stdout, stderr) {
    console.log("building " + tool + " for windows");
    exec('env GOOS=windows GOARCH=386 go build -o dist/win/' + tool + ".exe src/" + tool + "/" + tool + ".go", function(err, stdout, stderr) {
      console.log("building " + tool + " for linux");
      exec('env GOOS=linux GOARCH=386 go build -o dist/linux386/' + tool + " src/" + tool + "/" + tool + ".go", function(err, stdout, stderr) {
        cb();
      });
    });
  });
}
//======================================================================================================================
function buildAll(i, cb) {
  if (i >= tools.length) {
    console.log("build done");
    cb();
    return;
  }
  if (typeof(i) === 'undefined') i = 0;
  buildTool(tools[i], function() {
    i++;
    buildAll(i, cb);
  });
}
//======================================================================================================================
gulp.task('local-dist', ['local-push-ver'], function(cb) {
  buildAll(0, function() {
    console.log("compressing mac tools");
    exec("zip -D -r ./dist/kwutils_mac.zip ./dist/mac", function(err, stdout, stderr) {
      console.log("compressing windows tools");
      exec("zip -D -r ./dist/kwutils_win.zip ./dist/win", function(err, stdout, stderr) {
        console.log("compressing linux tools");
        exec("zip -D -r ./dist/kwutils_linux386.zip ./dist/linux386", function(err, stdout, stderr) {
          console.log("package created");
          cb();
        });
      });
    });
  });
});
//======================================================================================================================
gulp.task('local-upload-kw', ['local-dist'], function(cb) {
  console.log("uploading linux package");
  exec('./dist/mac/kwupload -t="./token.demo.kiteworks.com.json" -n="./dist/kwutils_linux386.zip" -p="My Folder/kwutils"', function(err, stdout, stderr) {
    console.log("linux package uploaded");
    console.log("uploading windows package");
    exec('./dist/mac/kwupload -t="./token.demo.kiteworks.com.json" -n="./dist/kwutils_win.zip" -p="My Folder/kwutils"', function(err, stdout, stderr) {
      console.log("windows package uploaded");
      console.log("uploading mac package");
      exec('./dist/mac/kwupload -t="./token.demo.kiteworks.com.json" -n="./dist/kwutils_mac.zip" -p="My Folder/kwutils"', function(err, stdout, stderr) {
        console.log("mac package uploaded");
        console.log("uploading user guide");
        exec('./dist/mac/kwupload -t="./token.demo.kiteworks.com.json" -n="./dist/user guide.pdf" -p="My Folder/kwutils"', function(err, stdout, stderr) {
          console.log("user guide uploaded");
          cb();
        });
      });
    });
  });
});
//======================================================================================================================
gulp.task('local-inc-ver-patch', function() {
  return gulp.src(['./version.json']).pipe(bump({
    type: 'patch'
  })).pipe(gulp.dest('./'));
});
//======================================================================================================================
gulp.task('local-inc-ver-minor', function() {
  return gulp.src(['./version.json']).pipe(bump({
    type: 'minor'
  })).pipe(gulp.dest('./'));
});
//======================================================================================================================
gulp.task('local-inc-ver-major', function() {
  return gulp.src(['./version.json']).pipe(bump({
    type: 'major'
  })).pipe(gulp.dest('./'));
});
//======================================================================================================================
gulp.task('local-push-ver', ['local-inc-ver-patch'], function() {
  return gulp.src(['./src/kw/kw.go']).pipe(bump({
    version: getVersion()
  })).pipe(gulp.dest('./src/kw/'));
});
//======================================================================================================================
gulp.task('git-commit', ['local-dist'], function() {
  return gulp.src(['./src/**', './gulpfile.js', './version.json']).pipe(git.commit(getVersion()));
});
//======================================================================================================================
gulp.task('git-push', ['git-commit'], function() {
  return git.push('origin', 'master', function(err) {
    if (err) throw err;
  });
});
//======================================================================================================================
gulp.task('git-tag', ['git-push'], function() {
  return git.tag(getVersion(), 'version');
});
//======================================================================================================================
gulp.task('default', ['local-inc-ver-patch', 'local-push-ver', 'local-dist', 'git-commit', 'git-push', 'git-tag', 'local-upload-kw']);
//======================================================================================================================
