package main

// =========================================================================================================
import "flag"

import "fmt"
import "../kw"

// =========================================================================================================
func main() {
	tokenFilePrt := flag.String("t", "./token.json", "The token json file path & name")
	kw.HandleFlags();

	t := kw.Load(*tokenFilePrt)

	jsonParsed := kw.Me(t)

	if jsonParsed != nil {
		fmt.Println(" ")

		fmt.Println("ID:         ", jsonParsed.Path("id").Data().(float64))
		fmt.Println("Active:     ", jsonParsed.Path("active").Data().(bool))
		fmt.Println("Suspended:  ", jsonParsed.Path("suspended").Data().(bool))
		fmt.Println("Created:    ", jsonParsed.Path("created").Data().(string))
		fmt.Println("Deleted:    ", jsonParsed.Path("deleted").Data().(bool))
		fmt.Println("Email:      ", jsonParsed.Path("email").Data().(string))
		fmt.Println("Base Dir ID:", jsonParsed.Path("basedirId").Data().(float64))
		fmt.Println("My Dir ID:  ", jsonParsed.Path("mydirId").Data().(float64))
		jsonParsed2 := kw.FolderProperties(t, int(jsonParsed.Path("syncdirId").Data().(float64)))
		fmt.Println("Sync Dir ID:", jsonParsed.Path("syncdirId").Data().(float64), "("+jsonParsed2.Path("name").Data().(string)+")")
		fmt.Println("Name:       ", jsonParsed.Path("name").Data().(string))
		fmt.Println("Verified:   ", jsonParsed.Path("verified").Data().(bool))
		fmt.Println("Internal:   ", jsonParsed.Path("internal").Data().(bool))

		fmt.Println(" ")

	}
}

// =========================================================================================================
