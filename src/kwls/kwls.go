package main

// =========================================================================================================
import "flag"

import "fmt"
import "../kw"
import "text/tabwriter"
import "strconv"
import "os"

// =========================================================================================================
func main() {
	tokenFilePrt := flag.String("t", "./token.json", "The token json file path & name")
	pathPrt := flag.String("p", "", "The path to retrieve. Leave blank for root folder")
	delPrt := flag.Bool("d", false, "List deleted content")
	kw.HandleFlags();

	t := kw.Load(*tokenFilePrt)

	var p int
	if *pathPrt == "" {
		p = -1
	} else {
		p = kw.FolderIdForPath(t, *pathPrt, 0)
		if p == -1 {
			fmt.Println("Path not found.")
			return
		}
	}

	jsonParsed := kw.FolderList(t, p)

	if jsonParsed != nil {
		w := tabwriter.NewWriter(os.Stdout, 0, 0, 1, ' ', tabwriter.Debug)
		children, _ := jsonParsed.S("data").Children()
		fmt.Println(" ")
		fmt.Fprintln(w, "Name\tId\tCreated\tModified\tT\tDeleted\t")
		fmt.Fprintln(w, " \t \t \t \t \t \t")
		for _, child := range children {
			if child.Path("deleted").Data().(bool) && !*delPrt {
				continue
			}
			fmt.Fprintln(w,
				child.Path("name").Data().(string)+
					"\t"+strconv.Itoa(int(child.Path("id").Data().(float64)))+
					"\t"+child.Path("created").Data().(string)+
					"\t"+child.Path("modified").Data().(string)+
					"\t"+child.Path("type").Data().(string)+
					"\t"+strconv.FormatBool(child.Path("deleted").Data().(bool))+
					"\t")
		}
		w.Flush()
		fmt.Println(" ")

	}
}

// =========================================================================================================
