package kw

// =========================================================================================================
import (
	"strconv"
	"github.com/Jeffail/gabs"
	"strings"
)
// =========================================================================================================
func FolderDelete(t Token, f int) (*gabs.Container){
	return Delete("/rest/folders/"+strconv.Itoa(f), t)
}
// =========================================================================================================
func FolderCreate(t Token, folderId int, name, dc string, sync bool, lifetime int) (*gabs.Container) {
	json := `{"name": "`+name+`","description": "`+dc+`","syncable": `+strconv.FormatBool(sync)+`,"fileLifetime": `+strconv.Itoa(lifetime)+`}`
	return Post("/rest/folders/"+strconv.Itoa(folderId)+"/folders?returnEntity=true", json, "application/json", t)
}
// =========================================================================================================
func FolderList(t Token, f int) (*gabs.Container) {
	if f==-1 {
		return Get("/rest/folders/top", t)
	} else {
		return Get("/rest/folders/"+strconv.Itoa(f)+"/children", t)
	}
}
// =========================================================================================================
func FolderListTop(t Token) (*gabs.Container) {
	return Get("/rest/folders/top", t)
}
// =========================================================================================================
func FolderProperties(t Token, id int) (*gabs.Container) {
	return Get("/rest/folders/"+strconv.Itoa(id),t)
}
// =========================================================================================================
func FolderMemberAdd(t Token, folderId int, emails string, notify bool, roleName string) (*gabs.Container) {
	emails = strings.Replace(emails," ","",-1)
	emails = strings.Replace(emails,",","\",\"",-1)
	if emails !=""  {emails ="\""+emails+"\""}
	roleId := RoleId(t, roleName)
	if roleId==-1 {
		jsonParsed, _ := gabs.ParseJSON([]byte(`{"status":404,"message":"role not found"}`))
		return jsonParsed
	}
	json := `{"notify": `+strconv.FormatBool(notify)+`,"emails": [`+emails+`],"roleId": `+strconv.Itoa(roleId)+`}`
	return Post("/rest/folders/"+strconv.Itoa(folderId)+"/members?returnEntity=true", json, "application/json", t)

}
// =========================================================================================================
func FolderIdForPath(t Token, path string, id int) (int) {
	var jsonParsed *gabs.Container

	if id==0 {
		jsonParsed = FolderListTop(t)
	} else {
		jsonParsed = FolderList(t,id)
	}

	if jsonParsed == nil {
		return -1
	}

	children, _ := jsonParsed.S("data").Children()

	for _, child := range children {
		if !child.Exists("path") {continue}
		jsonPath := child.Path("path").Data().(string)
		jsonId   := int(child.Path("id").Data().(float64))

		if jsonPath == path {
			return jsonId
		} else {
		}
		if strings.HasPrefix(path, jsonPath) {
			return FolderIdForPath (t,path,jsonId)
		}

	}
	return -1
}
// =========================================================================================================
