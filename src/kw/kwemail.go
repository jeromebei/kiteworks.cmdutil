package kw

// =========================================================================================================
import (
	"strconv"
	"github.com/Jeffail/gabs"
	"time"
	"strings"
)

// =========================================================================================================
func FileRequest(t Token, to,subject,body string, auth, viewOnly bool,files string, encrypt bool,count int, folder int) (*gabs.Container) {
	to = strings.Replace(to," ","",-1)
	to = strings.Replace(to,",","\",\"",-1)
	if to !=""  {to ="\""+to+"\""}

	files = strings.Replace(files," ","",-1)
	files = strings.Replace(files,",","\",\"",-1)
	if files !=""  {files ="\""+files+"\""}

	actionId:="2"
	if viewOnly {actionId="1"}
	json := `{
		  "to": [`+to+`],
		  "subject": "`+subject+`",
		  "body": "`+body+`",
		  "count": `+strconv.Itoa(count)+`,
		  "requireAuth": `+strconv.FormatBool(auth)+`,
		  "actionId": "`+actionId+`",
		  "files": [`+files+`],
		  "secureBody": `+strconv.FormatBool(encrypt)+`
		}`
		return Post("/rest/folders/"+strconv.Itoa(folder)+"/actions/requestFile", json, "application/json", t)
}
// =========================================================================================================
func SendEmail(t Token, to,cc,bcc,files string, acl bool,expire int, watermark string, secureBody bool,selfCopy bool, body,subject string) (*gabs.Container) {
	to = strings.Replace(to," ","",-1)
	to = strings.Replace(to,",","\",\"",-1)
	if to !=""  {to ="\""+to+"\""}

	cc = strings.Replace(cc," ","",-1)
	cc = strings.Replace(cc,",","\",\"",-1)
	if cc !=""  {cc ="\""+cc+"\""}

	bcc = strings.Replace(bcc," ","",-1)
	bcc = strings.Replace(bcc,",","\",\"",-1)
	if bcc !=""  {bcc ="\""+bcc+"\""}

	files = strings.Replace(files," ","",-1)
	files = strings.Replace(files,",","\",\"",-1)
	if files !=""  {files ="\""+files+"\""}

	verify := "no_auth"
	if acl {verify = "verify_recipient"}

	if watermark != "" {watermark = `"watermark": "`+watermark+`",`}

	tm := time.Now().AddDate(0,0,expire)
	expireFormat :=""
	if expire > 0 { expireFormat = `"expire": "`+tm.Format(time.RFC3339)+`",`}

	json := `{
		  "to": [`+to+`],
		  "cc": [`+cc+`],
		  "bcc": [`+bcc+`],
		  "files": [`+files+`],
		  "acl": "`+verify+`",`+expireFormat+`
		  "draft": false,
		  "preview": false,`+watermark+`
		  "secureBody": `+strconv.FormatBool(secureBody)+`,
		  "selfCopy": `+strconv.FormatBool(selfCopy)+`,
		  "body": "`+body+`",
		  "subject": "`+subject+`"
		}`

		//fmt.Println(json)

	return Post("/rest/mail/actions/sendFile?returnEntity=true", json, "application/json", t)

}
// =========================================================================================================
