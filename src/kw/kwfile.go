package kw

// =========================================================================================================
import (
	"crypto/tls"
	"io"
	"io/ioutil"
	"net/http"
	"os"
	"strconv"
	"bytes"
	"github.com/Jeffail/gabs"
	"mime/multipart"
	"path/filepath"
)
// =========================================================================================================
func FileDelete(t Token, f int) (*gabs.Container){
	return Delete("/rest/files/"+strconv.Itoa(f), t)
}
// =========================================================================================================
func FileDownload(t Token, fileId int, destination string) (*gabs.Container) {

	tr := &http.Transport{TLSClientConfig: &tls.Config{InsecureSkipVerify: true}}
	client := &http.Client{Transport: tr}
	req, _ := http.NewRequest("GET", t.Server+"/rest/files/"+strconv.Itoa(fileId)+"/content", nil)
	req.Header.Add("X-Accellion-Version", API_VERSION)
	req.Header.Add("Accept", "application/json")
	req.Header.Add("authorization", "Bearer "+t.AccessToken)
	res, err := client.Do(req)
	check(err)
	body, err := ioutil.ReadAll(res.Body)
	check(err)
	jsonParsed, err := gabs.ParseJSON([]byte(`{"status":`+strconv.Itoa(res.StatusCode)+`}`))
	check(err)
	if res.StatusCode==200 {
		err := ioutil.WriteFile(destination, body, 0644)
		check(err)
	}
	return jsonParsed
}
// =========================================================================================================
func FileUpload(t Token, folderId int, name string) (*gabs.Container) {
	file, err := os.Open(name)
	check(err)
	defer file.Close()
	frmbody := &bytes.Buffer{}
	writer := multipart.NewWriter(frmbody)
	part, err := writer.CreateFormFile("file", filepath.Base(name))
	check(err)
	_, err = io.Copy(part, file)
	err = writer.Close()
	check(err)
	req, err := http.NewRequest("POST", t.Server+"/rest/folders/"+strconv.Itoa(folderId)+"/actions/file?returnEntity=true", frmbody)
	check(err)

   req.Header.Set("Content-Type", writer.FormDataContentType())
	req.Header.Add("X-Accellion-Version", API_VERSION)
	req.Header.Add("Accept", "application/json")
	req.Header.Add("authorization", "Bearer "+t.AccessToken)

	tr := &http.Transport{TLSClientConfig: &tls.Config{InsecureSkipVerify: true}}
	client := &http.Client{Transport: tr}

	res, err := client.Do(req)
	check(err)
	body, err := ioutil.ReadAll(res.Body)
	check(err)
	jsonParsed, err := gabs.ParseJSON(body)
	check(err)
	return jsonParsed
}
// =========================================================================================================
func FileProperties(t Token, id int) (*gabs.Container) {
	return Get("/rest/files/"+strconv.Itoa(id),t)
}
// =========================================================================================================
func FileIdForPath(t Token, fp string) (int) {
	_, file := filepath.Split(fp)
	dir := filepath.Dir(fp)
	pathId:=FolderIdForPath(t,dir,0)
	if pathId==-1 {return pathId}
	fileId:=FileIdForName(t,file,pathId)
	return fileId
}
// =========================================================================================================
func FileIdForName(t Token, filename string, id int) (int) {
	var jsonParsed *gabs.Container

	if id==0 {
		jsonParsed = FolderListTop(t)
	} else {
		jsonParsed = FolderList(t,id)
	}

	if jsonParsed == nil {
		return -1
	}

	children, _ := jsonParsed.S("data").Children()

	for _, child := range children {
		jsonName := child.Path("name").Data().(string)
		jsonId   := int(child.Path("id").Data().(float64))

		if jsonName == filename {
			return jsonId
		}
	}
	return -1
}
// =========================================================================================================
