package kw

// =========================================================================================================
import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"github.com/Jeffail/gabs"
	"net/url"
	"time"
)
// =========================================================================================================
type Token struct {
	AccessToken  string `json:"access_token"`
	ClientId  	 string `json:"clientId"`
	ClientSecret string `json:"clientSecret"`
	Expires      int64  `json:"expires_in"`
	TokenType    string `json:"token_type"`
	Scope        string `json:"scope"`
	RefreshToken string `json:"refresh_token"`
	Server       string `json:"server"`
	Generated    int64  `json:"generated"`
}
// =========================================================================================================
func Test(t Token) (*gabs.Container) {
	return FolderListTop(t)
}
// =========================================================================================================
func RefreshToken(t Token) (Token) {
	form := url.Values{}
	form.Add("grant_type", "refresh_token")
	form.Add("client_id", t.ClientId)
	form.Add("client_secret", t.ClientSecret)
	form.Add("refresh_token", t.RefreshToken)

	jsonParsed := Post("/oauth/token", form.Encode(), "application/x-www-form-urlencoded", t)

	var value string
	if jsonParsed.Exists("error") {
		value, _ = jsonParsed.Path("error").Data().(string)
		fmt.Println("error: "+value)
		os.Exit(1)
	}
	value, _ = jsonParsed.Path("access_token").Data().(string)
	t.AccessToken = value
	value, _ = jsonParsed.Path("refresh_token").Data().(string)
	t.RefreshToken = value
	var value2 float64
	value2, _ = jsonParsed.Path("expires_in").Data().(float64)

	t.Expires = int64(value2)
	t.Generated = int64(time.Now().Unix())

	return t
}
// =========================================================================================================
func Load(tokenFile string) Token {
	raw, err := ioutil.ReadFile(tokenFile)
	if err != nil {
		fmt.Println("Unable to load token",tokenFile,"(",err,")")
		os.Exit(1)
	}
	var t Token
	json.Unmarshal(raw, &t)
	return t
}
// =========================================================================================================
func Save(t Token, tokenFile string) {
	b, err := json.Marshal(t)
	if err != nil {
		fmt.Println("Unable to save token",tokenFile,"(",err,")")
		os.Exit(1)
	}
	ioutil.WriteFile(tokenFile, b, 0644)
}
// =========================================================================================================
