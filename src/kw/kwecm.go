package kw

// =========================================================================================================
import (
	"github.com/Jeffail/gabs"
	"strings"
)
// =========================================================================================================
func EcmFolderIdForPath(t Token, path string, id string) (string) {
	var jsonParsed *gabs.Container

  attrib :="path"
	if id=="0" {
		jsonParsed = EcmFolderListTop(t)
    attrib="name"
	} else {
		jsonParsed = EcmFolderList(t,id)
	}

	if jsonParsed == nil {
		return "-1"
	}

	children, _ := jsonParsed.S("data").Children()

	for _, child := range children {
		if !child.Exists(attrib) {continue}
		jsonPath := child.Path(attrib).Data().(string)
		jsonId   := child.Path("id").Data().(string)

		if jsonPath == path {
			return jsonId
		} else {
		}
		if strings.HasPrefix(path, jsonPath) {
			return EcmFolderIdForPath (t,path,jsonId)
		}

	}
	return "-1"
}
// =========================================================================================================
func EcmFolderList(t Token, f string) (*gabs.Container) {
	if f=="-1" {
		return Get("/rest/sources", t)
	} else {
		return Get("/rest/sources/"+f+"/children", t)
	}
}
// =========================================================================================================
func EcmFolderListTop(t Token) (*gabs.Container) {
	return Get("/rest/sources", t)
}
// =========================================================================================================
