package kw

// =========================================================================================================
import (
	"crypto/tls"
	"fmt"
	"os"
	"io/ioutil"
	"net/http"
	"strconv"
	"bytes"
	"github.com/Jeffail/gabs"
	"flag"
)
// =========================================================================================================
const VERSION = "1.1.32"
const API_VERSION = "7"
// =========================================================================================================
func Get(uri string, t Token) (*gabs.Container) {
	tr := &http.Transport{TLSClientConfig: &tls.Config{InsecureSkipVerify: true}}
	client := &http.Client{Transport: tr}
	req, _ := http.NewRequest("GET", t.Server+uri, nil)
	req.Header.Add("X-Accellion-Version", API_VERSION)
	req.Header.Add("Accept", "application/json")
	req.Header.Add("authorization", "Bearer "+t.AccessToken)
	res, err := client.Do(req)
	check(err)
	body, err := ioutil.ReadAll(res.Body)
	check(err)
	jsonParsed, err := gabs.ParseJSON(body)
	check(err)
	return jsonParsed
}
// =========================================================================================================
func Delete(uri string, t Token) (*gabs.Container) {
	tr := &http.Transport{TLSClientConfig: &tls.Config{InsecureSkipVerify: true}}
	client := &http.Client{Transport: tr}
	req, _ := http.NewRequest("DELETE", t.Server+uri, nil)
	req.Header.Add("X-Accellion-Version", API_VERSION)
	req.Header.Add("Accept", "application/json")
	req.Header.Add("authorization", "Bearer "+t.AccessToken)
	res, err := client.Do(req)
	check(err)
	_, err = ioutil.ReadAll(res.Body)
	check(err)
	jsonParsed, err := gabs.ParseJSON([]byte(`{"status":`+strconv.Itoa(res.StatusCode)+`}`))
	check(err)
	return jsonParsed
}
// =========================================================================================================
func Post(uri string, json string, ctype string, t Token) (*gabs.Container) {
	tr := &http.Transport{TLSClientConfig: &tls.Config{InsecureSkipVerify: true}}
	client := &http.Client{Transport: tr}
	req, _ := http.NewRequest("POST", t.Server+uri, bytes.NewBuffer([]byte(json)))
	req.Header.Add("X-Accellion-Version", API_VERSION)
	req.Header.Add("Accept", "application/json")
	req.Header.Add("Content-Type", ctype)
	req.Header.Add("authorization", "Bearer "+t.AccessToken)
	res, err := client.Do(req)
	check(err)
	body, err := ioutil.ReadAll(res.Body)
	check(err)
	jsonParsed, err := gabs.ParseJSON(body)
	if err != nil {
		//panic(err.Error())
		jsonParsed, err = gabs.ParseJSON([]byte(`{"status":"`+strconv.Itoa(res.StatusCode)+`"}`))
		return jsonParsed
	}
	return jsonParsed
}
// =========================================================================================================
func HandleFlags() {
	ver := flag.Bool("ver", false, "Display version information")
	flag.Parse()
	if *ver {
		fmt.Println()
		fmt.Println("kiteworks command line tools version",VERSION)
		fmt.Println()
		os.Exit(0)
	}
}
// =========================================================================================================
func check(e error) {
    if e != nil {
		 fmt.Println("")
        panic(e)
    }
}
// =========================================================================================================
