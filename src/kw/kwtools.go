package kw

// =========================================================================================================
import (
	"github.com/Jeffail/gabs"
	"strconv"
)
// =========================================================================================================
func Me(t Token) (*gabs.Container){
	return Get("/rest/users/me", t)
}
// =========================================================================================================
func RoleId (t Token, name string) (int) {
	// get the role id
	rolesData := Get("/rest/roles",t)
	roles, _ := rolesData.S("data").Children()
	for _, role := range roles {
		roleName := role.Path("name").Data().(string)
		roleId   := int(role.Path("id").Data().(float64))
		if roleName==name {
			return roleId
		}
	}
	return -1
}
// =========================================================================================================
func Comment(t Token, file int, comment string) (*gabs.Container) {
	json := `{
		  "contents": "`+comment+`"
		}`
		return Post("/rest/files/"+strconv.Itoa(file)+"/comments?returnEntity=true", json, "application/json", t)
}
// =========================================================================================================
