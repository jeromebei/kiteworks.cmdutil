package main

// =========================================================================================================
import "flag"

import "fmt"
import "../kw"
import "text/tabwriter"
import "os"
import "strconv"

// =========================================================================================================
func main() {
	tokenFilePrt := flag.String("t", "./token.json", "The token json file path & name")
	pathPrt := flag.String("p", "", "The ecm path to retrieve. Leave blank for a list of ecm sources")
  delPrt := flag.Bool("d", false, "List deleted content")
	kw.HandleFlags();

	t := kw.Load(*tokenFilePrt)

	var p string
	if *pathPrt == "" {
		p = "-1"
	} else {
		p = kw.EcmFolderIdForPath(t, *pathPrt, "0")
		if p == "-1" {
			fmt.Println("Path not found.")
			return
		}
	}

	jsonParsed := kw.EcmFolderList(t, p)

	if jsonParsed != nil {
		w := tabwriter.NewWriter(os.Stdout, 0, 0, 1, ' ', tabwriter.Debug)
		children, _ := jsonParsed.S("data").Children()
    fmt.Println(" ")

    if *pathPrt == "" || !jsonParsed.Path("data.created").Exists(){
  		fmt.Fprintln(w, "Name\tId\t")
  		fmt.Fprintln(w, " \t \t")
  		for _, child := range children {
  			fmt.Fprintln(w,
  				child.Path("name").Data().(string)+
  					"\t"+child.Path("id").Data().(string)+
  					"\t")
  		}
    } else {
      fmt.Fprintln(w, "Name\tId\tCreated\tModified\tT\tDeleted\t")
  		fmt.Fprintln(w, " \t \t \t \t \t \t")
  		for _, child := range children {
  			if child.Path("deleted").Data().(bool) && !*delPrt {
  				continue
  			}
  			fmt.Fprintln(w,
  				child.Path("name").Data().(string)+
  					"\t"+child.Path("id").Data().(string)+
  					"\t"+child.Path("created").Data().(string)+
  					"\t"+child.Path("modified").Data().(string)+
  					"\t"+child.Path("type").Data().(string)+
  					"\t"+strconv.FormatBool(child.Path("deleted").Data().(bool))+
  					"\t")
  		}
    }
		w.Flush()
		fmt.Println(" ")

	}
}

// =========================================================================================================
