package main

// =========================================================================================================
import "flag"

import "fmt"
import "strings"
import "strconv"
import "../kw"

// =========================================================================================================
func main() {
   tokenFilePrt := flag.String("t", "./token.json", "The token json file path & name")
	to := flag.String("to", "", "The list of email recipients, separated by a comma")
	s  := flag.String("s", "", "The email subject")
   b  := flag.String("b", "", "The email body")
	pa := flag.String("p", "", "The folder the recipient will upload files to. Note that the folder has to exist in kiteworks")
	a  := flag.Bool("na", false, "Do not require authentication")
	v  := flag.Bool("v", false, "View only (If not set, recipient(s) can download files specified with the -f flag)")
   f  := flag.String("f", "", "The files the recipient may view in the upload folder, separated by a comma - note that the files have to exist in the specified folder")
   en := flag.Bool("en", false, "Encrypt the email body")
   c  := flag.Int("c", 5, "The maximum number of files the recipient can upload")
   kw.HandleFlags();

   t := kw.Load(*tokenFilePrt)

   var p int
	if *pa=="" {
      fmt.Println("Folder not specified.")
      return;
   } else {
		p=kw.FolderIdForPath(t,*pa,0)
		if p==-1 {
			fmt.Println("Folder not found.")
			return;
		}
	}


   files := ""
	fileNameArray := strings.Split(*f,",")
	for _, fileName := range fileNameArray {
	 fileName = strings.TrimSpace(fileName)
    if fileName=="" {continue}
    id := kw.FileIdForPath(t,*pa+"/"+fileName)
		if id==-1 {
			fmt.Println("File not found",fileName)
			return
		}
		files = files + strconv.Itoa(id)+","
	}

	if len(files) > 0 {files = files[:len(files)-1]}

	jsonParsed := kw.FileRequest(t, *to, *s, *b, *a, *v, files, *en, *c, p)

	if jsonParsed!=nil {
		if jsonParsed.Exists("errors") {
			children, _ := jsonParsed.S("errors").Children()
			for _, child := range children {
				fmt.Println("Error sending file request:",child.Path("message").Data().(string))
			}
    } else {
		fmt.Println("File request sent.")
    }
	} else {
    fmt.Println("An error occurred while parsing the output.")
  }
}

// =========================================================================================================
