package main

// =========================================================================================================
import "flag"

import "fmt"
import "../kw"

// =========================================================================================================
func main() {
	tokenFilePrt := flag.String("t", "./token.json", "The token json file path & name")
	pathPrt := flag.String("p", "", "The parent path. Leave blank to create a root folder")
	namePrt := flag.String("n", "", "The folder name")
	dcPrt := flag.String("d", "", "The folder description")
	syncPrt := flag.Bool("ns", false, "Prevent folder to be synced to a desktop")
	lifetimePrt := flag.Int("l", 0, "The file lifetime")
	kw.HandleFlags();

	t := kw.Load(*tokenFilePrt)

	var p int
	if *pathPrt == "" {
		p = -1
	} else {
		p = kw.FolderIdForPath(t, *pathPrt, 0)
		if p == -1 {
			fmt.Println("Parent path not found.")
			return
		}
	}

	jsonParsed := kw.FolderCreate(t, p, *namePrt, *dcPrt, *syncPrt, *lifetimePrt)

	if jsonParsed != nil {
		if jsonParsed.Exists("errors") {
			children, _ := jsonParsed.S("errors").Children()
			for _, child := range children {
				fmt.Println("Error creating folder:", child.Path("message").Data().(string))
			}
		} else {
			fmt.Println("Folder created.")
		}

	} else {
		fmt.Println("An error occurred while parsing the output.")

	}
}

// =========================================================================================================
