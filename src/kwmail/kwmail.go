package main

// =========================================================================================================
import "flag"
import "fmt"
import "strings"
import "strconv"
import "../kw"

// =========================================================================================================
func main() {
	tokenFilePrt := flag.String("t", "./token.json", "The token json file path & name")
	to := flag.String("to", "", "The list of email recipients, seperated by a comma")
	cc := flag.String("cc", "", "The list of email carbon copy recipients, separated by a comma")
	bcc := flag.String("bcc", "", "The list of email blind carbon copy recipients, separated by a comma")
	f := flag.String("f", "", "The file paths and names, separated by a comma - note that the files have to exist in kiteworks")
	s := flag.String("s", "", "The email subject")
	b := flag.String("b", "", "The email body")
	a := flag.Bool("a", false, "Access control - Only verified recipients can download the files")
	w := flag.String("w", "", "Optional watermark when viewing files online")
	e := flag.Int("e", 0, "Optional link expiry in days")
	en := flag.Bool("en", false, "Encrypt the email body")
	sc := flag.Bool("sc", false, "Copy the email to yourself")
	kw.HandleFlags();

	t := kw.Load(*tokenFilePrt)

	files := ""
	fileNameArray := strings.Split(*f, ",")

	for _, fileName := range fileNameArray {
		fileName = strings.TrimSpace(fileName)
		id := kw.FileIdForPath(t, fileName)
		if id == -1 {
			fmt.Println("File not found", fileName)
			return
		}
		files = files + strconv.Itoa(id) + ","
	}

	files = files[:len(files)-1]

	jsonParsed := kw.SendEmail(t, *to, *cc, *bcc, files, *a, *e, *w, *en, *sc, *b, *s)
	if jsonParsed != nil {
		if jsonParsed.Exists("errors") {
			children, _ := jsonParsed.S("errors").Children()
			for _, child := range children {
				fmt.Println("Error sending email:", child.Path("message").Data().(string))
			}
		} else {
			fmt.Println("Email sent.")
		}

	} else {
		fmt.Println("An error occurred while parsing the output.")

	}
}

// =========================================================================================================
