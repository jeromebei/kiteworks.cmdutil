package main

// =========================================================================================================
import "flag"

import "fmt"
import "os"
import "../kw"

// =========================================================================================================
func main() {
	tokenFilePrt 	:= flag.String("t", "./token.json", "The token json file path & name")
	pathPrt 			:= flag.String("p", "", "The parent path. Leave blank to create a root folder")
	namePrt 			:= flag.String("n", "", "The file name and path")
	kw.HandleFlags();

	t := kw.Load(*tokenFilePrt)

	if _, err := os.Stat(*namePrt); os.IsNotExist(err) {
		fmt.Println("File does not exist.")
		return
	}

	var p int
	if *pathPrt == "" {
		p = -1
	} else {
		p = kw.FolderIdForPath(t, *pathPrt, 0)
		if p == -1 {
			fmt.Println("Parent path not found.")
			return
		}
	}

	jsonParsed := kw.FileUpload(t, p, *namePrt)

	if jsonParsed != nil {
		if jsonParsed.Exists("errors") {
			children, _ := jsonParsed.S("errors").Children()
			for _, child := range children {
				fmt.Println("Error uploading file:", child.Path("message").Data().(string))
			}
		} else {
			fmt.Println("File uploaded.")
		}

	} else {
		fmt.Println("An error occurred while parsing the output.")

	}
}

// =========================================================================================================
