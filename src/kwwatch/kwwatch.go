package main

// =========================================================================================================
import "flag"
import "fmt"
import "github.com/fsnotify/fsnotify"
import "os/exec"
import "../kw"

// =========================================================================================================
func main() {
	f 		 := flag.String("f", "./", "The folder to watch")
	e 		 := flag.String("e", "", "The executable or shell script to trigger. Note that the first argument to the shell script will be the file path, the second argument will be the file name")
	kw.HandleFlags();

	if *e == "" {
		fmt.Println("no executable / shell script defined")
		return
	}

	watcher, err := fsnotify.NewWatcher()
	check(err)
	defer watcher.Close() 

	done := make(chan bool)
	go func() {
		for {
			select {
			case event := <-watcher.Events:
				if event.Op == fsnotify.Create {
					fmt.Println("new file:", event.Name)
					cmd := exec.Command(*e, *f, event.Name)
					err = cmd.Run()
					if err != nil {
						fmt.Println(err)
					}

				}
			case err := <-watcher.Errors:
				fmt.Println("error:", err)
			}
		}
	}()

	err = watcher.Add(*f)
	check(err)
	<-done

}

// =========================================================================================================
func check(e error) {
	if e != nil {
		fmt.Println("")
		panic(e)
	}
}

// =========================================================================================================
