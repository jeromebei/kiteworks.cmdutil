package main

// =========================================================================================================
import "flag"

import "fmt"
import "../kw"

// =========================================================================================================
func main() {
	tokenFilePrt := flag.String("t", "./token.json", "The token json file path & name")
	pathPrt := flag.String("p", "", "The full path to the file to remove")
	kw.HandleFlags();

	t := kw.Load(*tokenFilePrt)

	var p int
	if *pathPrt == "" {
		fmt.Println("File path not specified.")
		return
	} else {
		p = kw.FileIdForPath(t, *pathPrt)
		if p == -1 {
			fmt.Println("File path not found.")
			return
		}
	}

	jsonParsed := kw.FileDelete(t, p)

	if jsonParsed != nil {
		if jsonParsed.Exists("errors") {
			children, _ := jsonParsed.S("errors").Children()
			for _, child := range children {
				fmt.Println("Error removing file:", child.Path("message").Data().(string))
			}
		} else {
			fmt.Println("File removed.")
		}

	} else {
		fmt.Println("An error occurred while parsing the output.")

	}
}

// =========================================================================================================
