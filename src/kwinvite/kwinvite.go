package main

// =========================================================================================================
import "flag"

import "fmt"
import "../kw"

// =========================================================================================================
func main() {
	tokenFilePrt := flag.String("t", "./token.json", "The token json file path & name")
	pathPrt := flag.String("p", "", "The folder to invite members to")
	emails := flag.String("e", "", "The list of email addresses to invite, separated by comma")
	notify := flag.Bool("n", false, "Notify recipients")
	role := flag.String("r", "", "The role for the new members (Viewer, Downloader, Collaborator, Manager)")
	kw.HandleFlags();

	t := kw.Load(*tokenFilePrt)

	var p int
	if *pathPrt == "" {
		fmt.Println("Folder not specified.")
		return
	} else {
		p = kw.FolderIdForPath(t, *pathPrt, 0)
		if p == -1 {
			fmt.Println("Folder not found.")
			return
		}
	}

	jsonParsed := kw.FolderMemberAdd(t, p, *emails, *notify, *role)

	if jsonParsed != nil {
		if jsonParsed.Exists("errors") {
			children, _ := jsonParsed.S("errors").Children()
			for _, child := range children {
				fmt.Println("Error adding member:", child.Path("message").Data().(string))
			}
		} else {
			fmt.Println("Members added.")
		}
	} else {
		fmt.Println("An error occurred while parsing the output.")
	}
}

// =========================================================================================================
