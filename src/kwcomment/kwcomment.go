package main

// =========================================================================================================
import "flag"

import "fmt"
import "../kw"

// =========================================================================================================
func main() {
	tokenFilePrt := flag.String("t", "./token.json", "The token json file path & name")
	pathPrt := flag.String("p", "", "The path and file name to comment on")
	commentPrt := flag.String("c", "", "The comment to add")
	kw.HandleFlags();

	t := kw.Load(*tokenFilePrt)

	var p int
	if *pathPrt == "" {
		fmt.Println("No file specified.")
		return
	} else {
		p = kw.FileIdForPath(t, *pathPrt)
		if p == -1 {
			fmt.Println("File not found.")
			return
		}
	}

	jsonParsed := kw.Comment(t, p, *commentPrt)

	if jsonParsed != nil {
		if jsonParsed.Exists("errors") {
			children, _ := jsonParsed.S("errors").Children()
			for _, child := range children {
				fmt.Println("Error adding comment:", child.Path("message").Data().(string))
			}
		} else {
			fmt.Println("Commend added.")
		}
	} else {
		fmt.Println("An error occurred while parsing the output.")
	}
}

// =========================================================================================================
