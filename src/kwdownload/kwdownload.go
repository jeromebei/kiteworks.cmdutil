package main

// =========================================================================================================
import "flag"

import "fmt"
import "../kw"

// =========================================================================================================
func main() {
	tokenFilePrt := flag.String("t", "./token.json", "The token json file path & name")
	destinationPrt := flag.String("d", "./unknown.file", "The destination path & file name")
	pathPrt := flag.String("p", "", "The path and file name to download")
	kw.HandleFlags();

	t := kw.Load(*tokenFilePrt)

	var p int
	if *pathPrt == "" {
		fmt.Println("No file specified.")
		return
	} else {
		p = kw.FileIdForPath(t, *pathPrt)
		if p == -1 {
			fmt.Println("File not found.")
			return
		}
	}

	jsonParsed := kw.FileDownload(t, p, *destinationPrt)

	if jsonParsed != nil {
		if jsonParsed.Path("status").Data().(float64) == 200 {
			fmt.Println("File downloaded as", *destinationPrt)
		} else {
			fmt.Println("Error downloading file:", int(jsonParsed.Path("status").Data().(float64)))
		}
	} else {
		fmt.Println("An error occurred while parsing the output.")

	}
}

// =========================================================================================================
