package main

// =========================================================================================================
import "flag"

import "fmt"
import "../kw"
import "text/tabwriter"
import "strconv"
import "os"

// =========================================================================================================
func main() {
	tokenFilePrt 	:= flag.String("t", "./token.json", "The token json file path & name")
	contentPrt 		:= flag.String("c", "", "The content to search for")
	limitPrt 		:= flag.Int("l", 20, "The maximum number of results (limit)")
	delPrt 			:= flag.Bool("d", false, "Show deleted files")
	extended 		:= flag.Bool("e", false, "Extended file information")
	kw.HandleFlags();

	t := kw.Load(*tokenFilePrt)

	jsonParsed := kw.Search(t, *contentPrt, *limitPrt)

	if jsonParsed != nil {
		w := tabwriter.NewWriter(os.Stdout, 0, 0, 1, ' ', tabwriter.Debug)
		children, _ := jsonParsed.S("files").Children()

		fmt.Println(" ")

		if *extended {
			fmt.Fprintln(w, "Name\tPath\tId\tCreated\tModified\tDeleted\tFingerprint\tSize\tExpiry\tLink\t")
			fmt.Fprintln(w, " \t \t \t \t \t \t")
		} else {
			fmt.Fprintln(w, "Name\tPath\tSize\tLink\t")
			fmt.Fprintln(w, " \t \t \t \t")
		}

		for _, child := range children {
			if child.Path("deleted").Data().(bool) && !*delPrt {
				continue
			}
			props := kw.FolderProperties(t, int(child.Path("parentId").Data().(float64)))

			if *extended {
				fmt.Fprintln(w,
					child.Path("name").Data().(string)+
						"\t"+props.Path("path").Data().(string)+
						"\t"+strconv.Itoa(int(child.Path("id").Data().(float64)))+
						"\t"+child.Path("created").Data().(string)+
						"\t"+child.Path("modified").Data().(string)+
						"\t"+strconv.FormatBool(child.Path("deleted").Data().(bool))+
						"\t"+child.Path("fingerprint").Data().(string)+
						"\t"+strconv.Itoa(int(child.Path("size").Data().(float64)))+
						"\t"+strconv.Itoa(int(child.Path("expire").Data().(float64)))+
						"\t"+child.Path("permalink").Data().(string)+
						"\t")
			} else {
				fmt.Fprintln(w,
					child.Path("name").Data().(string)+
						"\t"+props.Path("path").Data().(string)+
						"\t"+strconv.Itoa(int(child.Path("size").Data().(float64)))+
						"\t"+child.Path("permalink").Data().(string)+
						"\t")
			}
		}
		w.Flush()
		fmt.Println(" ")

	}
}

// =========================================================================================================
