package main

// =========================================================================================================
import "flag"

import "fmt"
import "../kw"

// =========================================================================================================
func main() {
	tokenFilePrt := flag.String("t", "./token.json", "The token json file path & name")
	kw.HandleFlags();

	t := kw.Load(*tokenFilePrt)
	jsonParsed := kw.Test(t)
	if jsonParsed != nil {
		fmt.Println("result: success")

	} else {
		fmt.Println("result: failure")
	}

}

// =========================================================================================================
