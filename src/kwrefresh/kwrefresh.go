package main

// =========================================================================================================
import "flag"

import "fmt"
import "time"
import "../kw"

// =========================================================================================================
func main() {
	tokenFilePrt := flag.String("t", "./token.json", "The token json file path & name")
	kw.HandleFlags();

	t := kw.Load(*tokenFilePrt)
	t = kw.RefreshToken(t)
	kw.Save(t, *tokenFilePrt)

	ex := int64(t.Generated + t.Expires)
	timeStamp := time.Unix(ex, 0)

	fmt.Println("token refreshed successfully, it will be valid until", timeStamp)
}

// =========================================================================================================
