#!/bin/bash

files=( "kwupload" "kwmd" "kwls" "kwtest" "kwmail" "kwrefresh" "kwdownload" "kwme" "kwrd" "kwdel" "kwsearch" "kwinvite" "kwrequest" "kwwatch")

cd ./src/kwmd
for i in "${files[@]}"
do
   :
    cd ../$i
    echo Building $i for Mac
    go build -o ../../dist/mac/$i
    echo Building $i for Windows
    env GOOS=windows GOARCH=386 go build -o ../../dist/win/$i.exe
    echo Building $i for Linux
    env GOOS=linux GOARCH=386 go build -o ../../dist/linux386/$i
done
cd ../../dist
zip -r ./kwutils_win.zip win
zip -r ./kwutils_osx.zip mac
zip -r ./kwutils_linux386.zip linux386
